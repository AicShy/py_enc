from conf import refactor_str_from_enc, refactor_str_to_enc


# Функция шифрования
def encrypt(text, key):
    key = key.lower()
    result = ""
    for i in range(len(text)):
        char = text[i]
        if char.isupper():
            result += chr(((ord(char) - 1040) + (ord(key[i]) - 1040)) % 32 + 1040)
        else:
            result += chr(((ord(char) - 1072) + (ord(key[i]) - 1072)) % 32 + 1072)
        key += text[i]
    return result


# Функция расшифрования
def decrypt(text, key):
    key = key.lower()
    result = ""
    for i in range(len(text)):
        char = text[i]
        if char.isupper():
            result += chr(((ord(char) - 1040) - (ord(key[i]) - 1040)) % 32 + 1040)
        else:
            result += chr(((ord(char) - 1072) - (ord(key[i]) - 1072)) % 32 + 1072)
        key += result[i]

    return result


def main():
    print("Шифр Виженера")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    while True:
        key = input("Введите ключ-букву: ")
        if len(key) == 1:
            break
        else:
            print('Ключ должен быть одним символом')
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text, key))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text, key)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text, key)))
            print("Результат работы в файле new.txt")