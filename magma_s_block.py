from conf import blocks, reversed_blocks, str_to_hex, hex_to_str, refactor_str_to_enc, refactor_str_from_enc, check_if_hex, list_of_elem_len_two

# Функция шифрования
def encrypt(text):
    text = text.lower()
    result = ""
    # Замена по S-блоку
    for i in range(len(text)):
        result += blocks[i % 8][text[i]]
    return result


# Функция расшифрования
def decrypt(text):
    text = text.lower()
    result = ""
    # Обратная замена по S-блоку
    for i in range(len(text)):
        result += reversed_blocks[i % 8][text[i]]
    return result


def main():
    print("Шифр S-блок замены ГОСТ Р 34.12-2015")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if big_or_small == 1 and what_to_do == 1:
        hex_flag = int(input('Формат входных данных? (hex - 1, str  - 2): '))
    if big_or_small == 1 and what_to_do == 2:
        hex_flag = int(input('Формат выходных данных? (hex - 1, str  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            if hex_flag == 1:
                while True:
                    text = input('Введите строку в формате hex: ').upper()
                    if check_if_hex(text):
                        break
            elif hex_flag == 2:
                text = input('Введите строку: ')
                text = refactor_str_to_enc(text)
                text = str_to_hex(text)
                text = "".join(text)
            print("Шифрование: ", (encrypt(text)))
        if big_or_small == 2:
            file = open("test.txt", "r", encoding='utf-8')
            text = refactor_str_to_enc(file.read())
            text = str_to_hex(text)
            text = "".join(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(encrypt(text)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            while True:
                text = input('Введите строку в формате hex: ').upper()
                if check_if_hex(text):
                    break
            if hex_flag == 1:
                print("Расшифрование:", decrypt(text))
            elif hex_flag == 2:
                print("Расшифрование: ", refactor_str_from_enc(hex_to_str(list_of_elem_len_two(decrypt(text)))))
        if big_or_small == 2:
            file = open("output.txt", "r", encoding='utf-8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(str(refactor_str_from_enc(hex_to_str(list_of_elem_len_two(decrypt(text))))))
            print("Результат работы в файле new.txt")
