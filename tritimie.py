from conf import refactor_str_from_enc, refactor_str_to_enc, mark_upper_letter, unmark_upper_letter

# Функция шифрования
def encrypt(text):
    text = mark_upper_letter(text)
    result = ""
    for i in range(len(text)):
        # Запись смещенного символа (шифротекста) в результат
        result += chr(1072 + ((ord(text[i]) - 1072 + i) % 32))
    return result


def decrypt(text):
    result = ""
    for i in range(len(text)):
        # Запись смещенного символа (открытого текста) в результат
        result += chr(1072 + ((ord(text[i]) - 1072 - i) % 32))
    result = unmark_upper_letter(result)
    return result


def main():
    print("Шифр Тритемия")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text)))
            print("Результат работы в файле new.txt")
