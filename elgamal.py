from random import choice
from textwrap import wrap
from conf import refactor_str_to_enc, refactor_str_from_enc, is_prime, coprime, str_to_numbers, numbers_to_str


# Функция для генерации трех рандомизаторов
def gen_nums(n, amnt = 3):
    num = []
    nums = [i for i in range(1, n)]
    while len(num) != amnt:
        m = choice(nums)
        # Проверка на то, что рандомизатор еще не используется и на взаимную простоту рандомизатора и переданного числа (p - 1)
        if m not in num and coprime(m , n):
            num.append(m)
    return num


# Функция шифрования
def encrypt(text, p, g, x, test):
    y = (g**x) % p
    print("Открытые ключи: P = ", p, ", G = ", g, ", Y = ", y)
    print("Закрытый ключ:  X = ", x)
    if test:
        # В случае тестового ввода задание трех рандомизаторов
        nums = [3, 5, 7]
        l = 0
    else:
        # Получение трех рандомизаторов
        coprime_nums = gen_nums(p-1, 3)
    en_text = []
    for i in text:
        if test:
            # В случае тестового ввода рандомизаторы будут браться как 3,5,7,3,5,7...
            k = nums[l % 3]
            l += 1
        else:
            # Случайный выбор родного из рандомизаторов
            k = choice(coprime_nums)
        # Вычисление первого числа шифра (g^(k * i) (mod p))
        first_num = str((g ** k) % p).zfill(len(str(p)))
        # Вычисление второго числа шифра (y^(k * i) * Mi (mod p))
        second_num = str(((y ** k) * int(i)) % p).zfill(len(str(p)))
        en_text.extend([first_num, second_num])
    return ''.join(en_text)


# Функция расшифрования
def decrypt(text, p, x):
    result = []
    text = wrap(text, 4)
    for i in text:
        # Расшифрование шифротекста
        result.append(str((int(i[2:4]) * (int(i[:2]) ** (p - 1 - x))) % p))
    return result


def main():
    print("Шифр Elgamal")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            test = int(input('Тестовый ввод? (да - 1, нет  - 0): '))
            text = str(input("Введите открытый текст: "))
            text = refactor_str_to_enc(text)
            text = str_to_numbers(text)
        while True:
            p = input("Введите большое простое целое число P: ")
            if p.isdecimal() and is_prime(int(p)):
                p = int(p)
                break
            else:
                print("Число ", p, " не является простым")
        while True:
            g = input("Введите большое целое G, при этом G > 1 и G < P: ")
            if g.isdecimal():
                if 1 < int(g) < p:
                    g = int(g)
                    break
                else:
                    print("Число ", g, " не удволетворяет G > 1 и G < P")
            else:
                print("Число ", g, " не является целым")
        while True:
            x = input("Введите большое целое X, при этом X > 1 и X < P: ")
            if x.isdecimal():
                if 1 < int(x) < p:
                    x = int(x)
                    break
                else:
                    print("Число ", x, " не удволетворяет X > 1 и X < P")
                break
            else:
                print("Число ", x, " не является целым")
        if big_or_small == 1:
            print("Шифрование: ", encrypt(text, p, g, x, test))
        if big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = str_to_numbers(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, p, g, x, 0))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = str(input("Введите текст для расшифрования: "))
        while True:
            p = input("Введите большое простое целое число P: ")
            if p.isdecimal() and is_prime(int(p)):
                p = int(p)
                break
            else:
                print("Число ", p, " не является простым")
        while True:
            x = input("Введите большое целое X, при этом X > 1 и X < P: ")
            if x.isdecimal():
                if 1 < int(x) < p:
                    x = int(x)
                    break
                else:
                    print("Число ", x, " не удволетворяет X > 1 и X < P")
                break
            else:
                print("Число ", x, " не является целым")
        if big_or_small == 1:
            print(refactor_str_from_enc(numbers_to_str(decrypt(text, p, x))))
        if big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(numbers_to_str(decrypt(file.read(), p, x))))
            print("Результат работы в файле new.txt")
