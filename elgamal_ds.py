# Функция Эйлера
from sympy.ntheory import totient
from math import gcd as bltin_gcd
from ast import literal_eval
from conf import refactor_str_to_enc, is_prime, coprime


# Функция рассчета хэша
def generate_hash(text, n):
    h = [0]
    for char in text:
        h.append((h[-1] + (ord(char) - 1039))**2 % n)
    # print('Хэш:', h[-1])
    return h[-1]


#Функция перевода текста и его последующего хэширования
def convert_str_and_hash(text, p):
    text = refactor_str_to_enc(text)
    result = ""
    result = generate_hash(text, p)
    if result == 0:
        result = 1
    return result

# Функция генерации подписи
def generate_signature(text, G, K, P, X, p):
    m = convert_str_and_hash(text, p)
    # Вычисление числа a (а = G ^ K mod Р)
    a = (G ** K) % P
    # Вычисление числа b
    b = ((m - X * a % (P - 1)) * K ** (totient(P - 1) - 1)) % (P - 1)
    return [a, b]

# # Функция проверки подписи
def check_signature(text, a, b, Y, G, P, p):
    m = convert_str_and_hash(text, p)
    # Вычисление числа a1 (a1 = ((Y^a) * (a^b)) (mod Р))
    a1 = (Y**a*a**b) % P
    # Вычисление числа a2 (a2 = G^m (mod Р))
    a2 = G**m % P
    if a1 == a2:
        res = "Подпись верна"
    else:
        res = "Подпись не верна"
    return res


def main():
    print("ЭЦП Elgamal")
    what_to_do = int(input('Что делать? (подписать - 1, проверить подпись  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите сообщение: "))
        while True:
            P = int(input("Введите большое простое целое число P: "))
            if is_prime(P):
                break
            else:
                print("Число ", P, " не является простым")
        while True:
            G = int(input("Введите большое целое число G (G < P): "))
            if G < P:
                break
            else:
                print("Число ", G, " не удовлетворяет условию G < P")
        while True:
            X = int(input("Введите целое число X (1 < Х < Р): "))
            if 1 < X < P:
                break
            else:
                print("Число ", X, " не удовлетворяет условию 1 < Х < Р")
        Y = (G**X) % P
        print("Значение открытого ключа Y: ", Y)
        while True:
            K = int(input("Введите целое число K, такое что 1 < К < (Р-1) и K и P являются взаимно простыми: "))
            if not coprime(K, (P - 1)):
                print("Число ", K, " не удовлетворяет условию K и P являются взаимно простыми")
            elif 1 >= K and K >= (P - 1):
                print("Число ", K, " не удовлетворяет условию 1 < К < (Р-1)")
            else:
                break
        if big_or_small == 1:
            while True:
                p = int(input("Введите число p - модуль алгоритма хеширования: "))
                m = generate_hash(text, p)
                if 1 < m <= P - 1:
                    break
                else:
                    print("Вычисленный хэш ( ", m, " ) не удовлетворяет условию 1 < m ≤ (Р-1)")
            print("Получившаяся ЭЦП: ", generate_signature(text, G, K, P, X, p))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = file.read()
            while True:
                p = int(input("Введите число p - модуль алгоритма хеширования: "))
                m = generate_hash(text, p)
                if 1 < m <= P - 1:
                    break
                else:
                    print("Вычисленный хэш (", m, ") не удовлетворяет условию 1 < m ≤ (Р-1)")
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(generate_signature(text, G, K, P, X, p)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = str(input("Введите сообщение: "))
        while True:
            P = int(input("Введите большое простое целое число P: "))
            if is_prime(P):
                break
            else:
                print("Число ", P, " не является простым")
        while True:
            G = int(input("Введите большое целое число G (G < P): "))
            if G < P:
                break
            else:
                print("Число ", G, " не удовлетворяет условию G < P")
                continue
        Y = int(input("Значение открытого ключа Y: "))
        p = int(input("Введите простое число p - модуль алгоритма: "))
        if big_or_small == 1:
            while True:
                signature = input("Введите подпись в формате [a, b]: ")
                try:
                    signature = literal_eval(signature)
                except SyntaxError:
                    print("Неверно введена подпись")
                    continue
                if len(signature) == 2:
                    a = signature[0]
                    b = signature[1]
                    break
                else:
                    print("Введено не 2 элемента")
            print(check_signature(text, a, b, Y, G, P, p))
        if big_or_small == 2:
            text = open('test.txt', 'r',  encoding='UTF8').read()
            sign = literal_eval(open('output.txt', 'r',  encoding='UTF8').read())
            a = sign[0]
            b = sign[1]
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(check_signature(text, a, b, Y, G, P, p))
            print("Результат работы в файле new.txt")