from textwrap import wrap
from conf import refactor_str_to_enc, refactor_str_from_enc, is_prime, coprime, str_to_numbers, numbers_to_str

# Расширенный алгоритм Евклида
def get_inversed_mod(e, f):
    if e == 0:
        return (f, 0, 1)
    else:
        g, y, x = get_inversed_mod(f % e, e)
    return (g, x - (f // e) * y, y)


# Использование расширенного алгоритма Евклида для поиска d из сравнения ed ≡ 1(modf(n))
def inversed_mod(e, f):
    g, x, y = get_inversed_mod(e, f)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % f


# Функция шифрования
def encrypt(text, e, n):
    result = ""
    for i in text:
        result += str((int(i) ** e) % n).zfill(len(str(n)))
    return result


# Функция расшифрования
def decrypt(text, d, n):
    result = []
    text = wrap(text, (len(str(n))))
    for i in range(len(text)):
        result.append((int(text[i]) ** d) % n)
    return result

def main():
    print("Шифр RSA")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите текст для шифрования: "))
            text = refactor_str_to_enc(text)
            text = str_to_numbers(text)
        while True:
            p = int(input("Введите простое p: "))
            if is_prime(p):
                break
            else:
                print('Неверное p, введите еще раз, оно должно быть простым')
        while True:
            q = int(input("Введите простое число q: "))
            if is_prime(q):
                break
            else:
                print('Неверное q, введите еще раз, оно должно быть простым')
        n = p * q
        f = (p-1)*(q-1)
        print("n = ", n)
        print("f(n) = ", f)
        while True:
            e = int(input("Введите случайное целое число e, взаимно простое с f: "))
            if coprime(e, f):
                break
            else:
                print('Неверное e, введите еще раз, оно должно быть взаимно простом с f')
        d = inversed_mod(e, f)
        print("d = ", d)
        if big_or_small == 1:
            print("Шифрование: ", encrypt(text, e, n))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = str_to_numbers(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(encrypt(text, e, n)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input("Введите текст для расшифрования: ")
        d = int(input("Введите d: "))
        n = int(input("Введите n: "))
        if big_or_small == 1:
            print("Расшифрование:", refactor_str_from_enc(numbers_to_str(decrypt(text, d, n))))
        if big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(numbers_to_str(decrypt(file.read(), d, n))))
            print("Результат работы в файле new.txt")