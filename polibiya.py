from conf import refactor_str_from_enc, refactor_str_to_enc, list_of_elem_len_two, mark_upper_letter, unmark_upper_letter

# Функция шифрования
def encrypt(text):
    text = mark_upper_letter(text)
    result = ""
    alphabet = {'а': 11, 'б': 12, 'в': 13, 'г': 14, 'д': 15,
                'е': 16, 'ж': 21, 'з': 22, 'и': 23, 'й': 24,
                'к': 25, 'л': 26, 'м': 31, 'н': 32, 'о': 33,
                'п': 34, 'р': 35, 'с': 36, 'т': 41, 'у': 42,
                'ф': 43, 'х': 44, 'ц': 45, 'ч': 46, 'ш': 51,
                'щ': 52, 'ъ': 53, 'ы': 54, 'ь': 55, 'э': 56,
                'ю': 61, 'я': 62, '-': 63}
    # Замена по алфавиту
    for i in text:
        if i in alphabet.keys():
            result += str(alphabet[i])
    return result


def decrypt(text):
    result = ""
    alphabet = {11: 'а', 12: 'б', 13: 'в', 14: 'г', 15: 'д',
                16: 'е', 21: 'ж', 22: 'з', 23: 'и', 24: 'й', 25: 'к',
                26: 'л', 31: 'м', 32: 'н', 33: 'о', 34: 'п',
                35: 'р', 36: 'с', 41: 'т', 42: 'у', 43: 'ф',
                44: 'х', 45: 'ц', 46: 'ч', 51: 'ш', 52: 'щ',
                53: 'ъ', 54: 'ы', 55: 'ь', 56: 'э', 61: 'ю',
                62: 'я', 63: '-'}
    # Обратная замена по алфавиту
    for i in text:
        if int(i) in alphabet.keys():
            result += str(alphabet[int(i)])
    # Обратная обработка заглавных букв
    result = unmark_upper_letter(result)
    return result

def main():
    print("Шифр Квадрат Полибия")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text =  list_of_elem_len_two(input('Введите текст для расшифрования: '))
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = list_of_elem_len_two(file.read())
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text)))
            print("Результат работы в файле new.txt")
