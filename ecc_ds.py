from ast import literal_eval
from sympy.ntheory import totient
from conf import refactor_str_to_enc, is_prime, coprime


# Функция проверки параметров
def check_params(p, k, q, Xa = 1):
    if not is_prime(p):
        print('Неверное p, введите еще раз, оно должно быть простым')
        return False
    if k <= 0 or k >= q:
        print('Неверный k, введите еще раз, чтобы  0 < k < q')
        return False
    if Xa <= 0 or Xa >= q:
        print('Неверный Xa, введите еще раз, чтобы  0 < Xa < q')
        return False
    return True


# Функция проверки эллиптической кривой
def curve_check(a, b, p):
    if a < p:
        if (4 * (a ** 3) + 27 * (b ** 2)) % p != 0:
            return True
        else:
            print('Ваша кривая является особенной. Введите другие параметры')
            return False
    else:
        print('Введенный параметр а больше модуля p. Введите другие параметры')
        return False


# Функция проверки параметров подписи
def sign_check(r, s, q):
    if 0 >= r and r >= q:
        print('Неверный r')
        return False
    if 0 >= s and s >= q:
        print('Неверный s')
        return False
    return True


# Функция рассчета хэша
def generate_hash(text, n):
    h = [0]
    for char in text:
        h.append((h[-1] + (ord(char) - 1039))**2 % n)
    return h[-1]


# Функция для конвертации текста для шифрования и его последующего хэширования
def convert_str_and_hash(text, p):
    text = refactor_str_to_enc(text)
    result = ""
    result = generate_hash(text, p)
    if result == 0:
        result = 1
    return result


# Функция подсчета фуннкци Эйлера
def euler_function(n):
    res = 0
    if is_prime(n):
        res = n - 1
    else:
        for i in range(n):
            if coprime(i,n):
                res += 1
    return res


# Функция умножения точек
def doubling_P(P, a, p):
    lam = ['', '']
    lam[0] = (3 * P[0] ** 2 + a) % p
    lam[1] = 2 * P[1] % p
    if lam[1] == 0:
        lam = 0
    if not (lam[0] / lam[1]).is_integer():
        lam[0] = lam[0] % p
        lam[1] = (lam[1] ** (euler_function(p) - 1)) % p
        lam = int(lam[0] * lam[1] % p)
    else:
        lam = int(lam[0] / lam[1])
    x = (lam ** 2 - 2 * P[0]) % p
    y = (lam * (P[0] - x) - P[1]) % p
    return x, y


# Функция сложения точек
def addition_P(P1, P2, p):
    lam = ['', '']
    lam[0] = (P2[1] - P1[1]) % p
    lam[1] = (P2[0] - P1[0]) % p
    if lam[1] == 0:
        lam = 0
    elif not (lam[0] / lam[1]).is_integer():
        lam[0] = lam[0] % p
        lam[1] = (lam[1] ** (euler_function(p) - 1)) % p
        lam = int(lam[0] * lam[1] % p)
    else:
        lam = int(lam[0] / lam[1])
    x = (lam ** 2 - P1[0] - P2[0]) % p
    y = (lam * (P1[0] - x) - P1[1]) % p
    return x, y


# Функция генерации ключа
def key_gen(P, k, a, p):
    k = bin(k)[3:]
    P_ = P
    for i in k:
        P_ = doubling_P(P_, a, p)
        if i == '1':
            P_ = addition_P(P, P_, p)
    return P_


# Функция подписывания
def generate_signature(message, a, p, k, Xa, G, n, q):
    message = convert_str_and_hash(message, n)
    # Вычисление Ya = [Xa]G
    Ya = key_gen(G, Xa, a, p)
    print("Открытый ключ: ", Ya)
    # Вычисление P = [k]G
    P = key_gen(G, k, a, p)
    # Вычисление r = x mod q
    r = P[0] % q
    if r == 0:
        print("Ошибка, r = 0, выберите другое k")
        return "Error"
    # Вычисление s = (k * h + r * Xa) mod q
    s = (k * message + r * Xa) % q
    if s == 0:
        print("Ошибка, s = 0, выберите другое k")
        return "Error"
    res = [r, s]
    return res


# Функция проверки подписи
def check_signature(message, r, s, yu, p, a, G, q, n):
    message = convert_str_and_hash(message, n)
    # Вычисление u1
    u1 = s * message ** (totient(q) - 1) % q
    # Вычисление u2
    u2 = (-r * message ** (totient(q) - 1)) % q
    # Вычисление P1 = [u1]G
    P1 = key_gen(G, u1, a, p)
    # Вычисление P2 = [u2]Yu
    P2 = key_gen(yu, u2, a, p)
    # Вычисление P = [u1]G + [u2]Yu
    P = addition_P(P1, P2, p)
    if P[0] % q == r:
        res = 'Подпись верна'
    else:
        res = 'Подпись не верна'
    return res


def main():
    print("ЭЦП ГОСТ Р 34.10-2012")
    what_to_do = int(input('Что делать? (подписать - 1, проверить подпись  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите сообщение: "))
        while True:
            p = int(input("Введите простое p: "))
            q = int(input("Введите простое q: "))
            k = int(input("Введите k, при этом k > 0 и k < q: "))
            Xa = int(input("Введите Xa, при этом Xa > 0 и Xa < q: "))
            if check_params(p, k, q, Xa):
                break
        while True:
            G = input("Введите G в формате [x, y]: ")
            try:
                G = literal_eval(G)
            except SyntaxError:
                print("Неверно введен G")
                continue
            if len(G) == 2:
                break
            else:
                print("Введено не 2 элемента")
        while True:
            a = int(input("Введите a, при этом a < p: "))
            b = int(input("Введите b: "))
            if  curve_check(a, b, p):
                break
        n = int(input("Введите число n - модуль алгоритма хеширования: "))
        if big_or_small == 1:
            print("Подпись: ", generate_signature(text, a, p, k, Xa, G, n, q))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(generate_signature(text, a, p, k, Xa, G, n, q)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input("Введите сообщение: ")
        while True:
            p = int(input("Введите простое p: "))
            q = int(input("Введите простое q: "))
            k = int(input("Введите k, при этом k > 0 и k < q: "))
            if check_params(p, k, q):
                break
        while True:
            G = input("Введите G в формате [x, y]: ")
            try:
                G = literal_eval(G)
            except SyntaxError:
                print("Неверно введен G")
                continue
            if len(G) == 2:
                break
            else:
                print("Введено не 2 элемента")
        while True:
            a = int(input("Введите a, при этом a < p: "))
            b = int(input("Введите b: "))
            if  curve_check(a, b, p):
                break
        while True:
            Yu = input("Введите открытый ключ в формате [x1, x2]: ")
            try:
                Yu = literal_eval(Yu)
            except SyntaxError:
                print("Неверно введен открытый ключ")
                continue
            if len(Yu) == 2:
                break
            else:
                print("Введено не 2 элемента")
        n = int(input("Введите число n - модуль алгоритма хеширования: "))
        if big_or_small == 1:
            while True:
                r = int(input("Введите r (первый параметр подписи): "))
                s = int(input("Введите s (второй параметр подписи): "))
                if sign_check(r, s, q):
                    break
            print(check_signature(text, r, s, Yu, p, a, G, q, n))
        if big_or_small == 2:
            text = open('test.txt', 'r',  encoding='UTF8').read()
            sign = literal_eval(open('output.txt', 'r',  encoding='UTF8').read())
            r = sign[0]
            s = sign[1]
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(check_signature(text, r, s, Yu, p, a, G, q, n))
            print("Результат работы в файле new.txt")
