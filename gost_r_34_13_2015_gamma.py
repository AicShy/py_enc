from textwrap import wrap
import magma_s_block
from conf import del_text_from_itself, hex_to_bin, bin_to_hex, check_if_hex, refactor_str_to_enc, refactor_str_from_enc, str_to_hex, hex_to_str, add_to_text_itself, del_text_from_itself, list_of_elem_len_two


# Функция F режима простой замены
def f(S, X):
    for i in X:
        # Запись синхропосылки в накопители
        N1 = S[0]
        N2 = S[1]
        # Сложение значения накопителя с блоком ключа по модулю 2^32
        S1 = hex((int(N2, 2) + int(i, 2)) % (2 ** 32))[2:].zfill(8)
        # Замена по S-блоку
        S1 = magma_s_block.encrypt(S1)
        S1 = bin(int(S1, 16))[2:].zfill(32)
        # Сдвиг на 11 разрядов влево
        S1 = S1[11:] + S1[:11]
        # Сложение значения накопителя с новым значением по модулю 2
        S2 = bin(int(N1, 2) ^ int(S1, 2))[2:].zfill(32)
        N1 = bin(int(N2, 2))[2:].zfill(32)
        S[0] = N1
        N2 = S2
        S[1] = S2
    return N2, N1


# Функция (рас)шифрования
def encrypt(To, S, X):
    # Разбить полученный hex на блоки по 16 символов
    To = wrap(To, 16)
    new_To = ''
    # Перевод полученных блоков в bin
    for i in range(len(To)):
        new_To += hex_to_bin(To[i], 64)
    # Разбить получившующуюся bin последовательности на блоки по 64 символа
    To = wrap(new_To, 64)
    # Разбить полученный ключ в формате hex на блоки по 8 символов
    X = wrap(X, 8)
    # Развертывание ключа
    tmp_X = []
    for i in range(3):
        tmp_X.extend(X)
    tmp_X.extend(X[::-1])
    X = ''.join(tmp_X)
    X = hex_to_bin(X, 256)
    X = wrap(X, 32)
    # Обработка IV
    S = hex_to_bin(S, 32)
    S = S + (64 - len(S)) * "0"
    Tsh = ''
    # Начало шифрования
    for i in range(len(To)):
        S = wrap(S, 32)
        tmp_S = "".join(S)
        # Получение значения накопителей N1 и N2 при помощи шифрования в режиме простой замены
        N1, N2 = f(S, X)
        # Суммирование XOR открытого текста с полученными значениями
        Tsh += bin(int(To[i], 2) ^ int((N1 + N2), 2))[2:].zfill(64)
        # Изменение IV
        S = bin(int(tmp_S, 2) + 1)[2:].zfill(64)
    return bin_to_hex(Tsh, 16 * len(To))

def main():
    print("ГОСТ Р 34.12-2015 «Магма» (гаммирование ГОСТ Р 34.13-2015)")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if big_or_small == 1 and what_to_do == 1:
        hex_flag = int(input('Формат входных данных? (hex - 1, str  - 2): '))
    if big_or_small == 1 and what_to_do == 2:
        hex_flag = int(input('Формат выходных данных? (hex - 1, str  - 2): '))
    while True:
        key = input("Введите ключ в формате hex длиной 64: ").upper()
        if check_if_hex(key):
            if len(key) == 64:
                break
            else:
                print("Длина ключа должна быть 64")
    while True:
        IV = input("Введите вектор инициализации формате hex длиной 8: ").upper()
        if check_if_hex(IV):
            if len(IV) == 8:
                break
            else:
                print("Длина вектора инициализации должна быть 8")
    if what_to_do == 1:
        if big_or_small == 1:
            while True:
                if hex_flag == 1:
                    text = input('Введите строку в формате hex: ').upper()
                    if check_if_hex(text):
                        text = add_to_text_itself(text, 16)
                        break
                elif hex_flag == 2:
                    text = input('Введите строку: ')
                    text = refactor_str_to_enc(text)
                    text = add_to_text_itself(text, 8)
                    text = str_to_hex(text)
                    text = "".join(text)
                    break
            print("Шифрование: ", (encrypt(text, IV, key)))
        if big_or_small == 2:
            file = open("test.txt", "r", encoding='utf-8')
            text = refactor_str_to_enc(file.read())
            text = add_to_text_itself(text, 8)
            text = str_to_hex(text)
            text = "".join(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(encrypt(text, IV, key)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            while True:
                text = input('Введите строку в формате hex: ').upper()
                if check_if_hex(text):
                    break
            if hex_flag == 1:
                print("Расшифрование:", del_text_from_itself(encrypt(text, IV, key)))
            elif hex_flag == 2:
                print("Расшифрование: ", refactor_str_from_enc(del_text_from_itself(hex_to_str(list_of_elem_len_two(encrypt(text, IV, key))))))
        if big_or_small == 2:
            file = open("output.txt", "r", encoding='utf-8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(str(refactor_str_from_enc(del_text_from_itself(hex_to_str(list_of_elem_len_two(encrypt(text, IV, key)))))))
            print("Результат работы в файле new.txt")