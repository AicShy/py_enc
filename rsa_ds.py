from conf import refactor_str_to_enc, is_prime, coprime


# Расширенный алгоритм Евклида
def get_inversed_mod(e, f):
    if e == 0:
        return (f, 0, 1)
    else:
        g, y, x = get_inversed_mod(f % e, e)
        return (g, x - (f // e) * y, y)


# Использование расширенного алгоритма Евклида для поиска d из сравнения ed ≡ 1(modf(n))
def inversed_mod(e, f):
    g, x, y = get_inversed_mod(e, f)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % f


# Функция высчета хэша
def generate_hash(text, n):
    h = [0]
    for char in text:
        h.append((h[-1] + (ord(char) - 1039))**2 % n)
    return h[-1]


#Функция перевода текста и его последующего хэширования
def convert_str_and_hash(text, p):
    text = refactor_str_to_enc(text)
    result = generate_hash(text, p)
    if result == 0:
        result = 1
    return result


# Функция генерации подписи
def generate_signature(text, p, D, N):
    m = convert_str_and_hash(text, p)
    s = m**D % N
    return s


# Функция проверки подписи
def check_signature(text, E, p, S, N):
    new_hash = convert_str_and_hash(text, p)
    m = S ** E % N
    if new_hash == m:
        res = 'Подпись верна'
    else:
        res = 'Подпись не верна'
    return res


def main():
    print("ЭЦП RSA")
    what_to_do = int(input('Что делать? (подписать - 1, проверить подпись  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите сообщение: "))
        while True:
            P = int(input("Введите простое число P: "))
            if is_prime(P):
                break
            else:
                print("Число ", P, " не является простым")
        while True:
            Q = int(input("Введите простое число Q: "))
            if is_prime(Q):
                break
            else:
                print("Число ", Q, " не является простым")
        N = P * Q
        euler_function = (P-1) * (Q-1) # функция эйлера
        print("Значение числа N: ", N)
        print("Значение функции Эйлера: ", euler_function)
        while True:
            E = int(input("Введите целое число E, взаимно простое с функцией Эйлера: "))
            if coprime(E, euler_function):
                break
            else:
                print("Число ", E, " не является взаимнопростым с функцией Эйлера")
        D = inversed_mod(E, euler_function)
        print("Значение числа D: ", D)
        print("Получившийся открытый ключ: ", E, N)
        p = int(input("Введите число p - модуль алгоритма: "))
        if big_or_small == 1:
            print("Подпись: ", generate_signature(text, p, D, N))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(generate_signature(file.read(), p, D, N)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input("Введите сообщение: ")
        E = int(input("Введите число E: "))
        N = int(input("Введите число N: "))
        p = int(input("Введите простое число p - модуль алгоритма: "))
        if big_or_small == 1:
            S = int(input("Введите подпись S: "))
            print(check_signature(text, E, p, S, N))
        if big_or_small == 2:
            text = open('test.txt', 'r',  encoding='UTF8').read()
            S = int(open('output.txt', 'r',  encoding='UTF8').read())
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(check_signature(text, E, p, S, N))
            print("Результат работы в файле new.txt")