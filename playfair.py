from random import choice
from conf import alpabet, refactor_str_to_enc, refactor_str_from_enc, mark_upper_letter, unmark_upper_letter


# Функция конвертирования ключа из строки в шифротаблицу
def convert(string):
    list = []
    string = string.lower()
    string = string.replace(' ','')
    string = string.replace('ё','е')
    string = string.replace('ъ','ь')
    string = string.replace('й','и')
    list[:0] = string
    new_key = []
    for i in range(len(list)):
        if not list[i] in new_key:
            new_key.append(list[i])
    if len(new_key) > 30:
        new_key = new_key[:30]
    i = 0
    while len(new_key) != 30:
        if not alpabet[i] in new_key:
            new_key.append(alpabet[i])
        i += 1
    return new_key


# Функция шифрования
def encrypt(text, key):
    result = ''
    text = mark_upper_letter(text)
    text = text.replace("й", "икразткая")
    text = text.replace("ъ", "ттыердвилоыкм")
    i = 0
    # Дописывание буквы ф если в фразе идут две одинаковые буквы подряд
    while i < len(text):
        if i + 1 < len(text):
            if text[i] == text[i + 1]:
                text = text[:i + 1] + "ф" + text[i + 1:]
        elif i + 1 == len(text):
            text += 'ф'
        # Запись двух букв
        first_letter = key.index(text[i])
        second_letter = key.index(text[i + 1])
        # Проверка на то, что две буквы находятся в одной горизонтальной линии
        if (first_letter // 6 == second_letter // 6):
            # Запись сдвинутых вправо значений
            result += str(key[(first_letter + 1) % 6
                            + ((first_letter) // 6) * 6])
            result += str((key[(second_letter + 1) % 6
                            + ((second_letter) // 6) * 6]))
        # Проверка на то, что две буквы находятся в одной вертикальной линии
        elif (first_letter % 6 == second_letter % 6):
            # Запись сдвинутых вниз значений
            result += str(key[(first_letter + 6) % 30])
            result += str(key[(second_letter + 6) % 30])
        # Проверка на то, что буквы точно не находятся в одной линии
        elif (first_letter // 6 != second_letter // 6 and
                first_letter % 6 != second_letter % 6):
            # Проверка на то, что первая буква находится либо
            # слева и выше второй, либо
            # слева и ниже другой
            if (first_letter % 6 < second_letter % 6 and
                first_letter // 6 < second_letter // 6 or
                first_letter % 6 < second_letter % 6 and
                first_letter // 6 > second_letter // 6):
                # Запись сдвинутых значений
                result += str(key[first_letter
                            + abs(first_letter % 6
                            - second_letter % 6)])
                result += str(key[second_letter
                            - abs(second_letter % 6
                            - first_letter % 6)])
            # Проверка на то, что первая буква находится либо
            # справа и выше второй, либо
            # справа и ниже другой
            elif(first_letter % 6 > second_letter % 6 and
                first_letter // 6 < second_letter // 6 or
                first_letter % 6 > second_letter % 6 and
                first_letter // 6 > second_letter // 6):
                # Запись сдвинутых значений
                result += str(key[first_letter
                            - abs(first_letter % 6
                            - second_letter % 6)])
                result += str(key[second_letter
                            + abs(second_letter % 6
                            - first_letter % 6)])
        i += 2
    return result


# Функция расшифрования
def decrypt(text, key):
    result = ''
    i = 0
    while i < len(text):
        # Запись двух букв
        first_letter = key.index(text[i])
        second_letter = key.index(text[i + 1])
        # Проверка на то, что две буквы находятся в одной горизонтальной линии
        if (first_letter // 6 == second_letter // 6):
            result += str(key[(first_letter - 1) % 6
                            + ((first_letter) // 6) * 6])
            result += str((key[(second_letter - 1) % 6
                            + ((second_letter) // 6) * 6]))
        # Проверка на то, что буквы точно не находятся в одной линии
        elif (first_letter % 6 == second_letter % 6):
            result += str(key[(first_letter - 6) % 30])
            result += str(key[(second_letter - 6) % 30])
        # Проверка на то, что буквы точно не находятся в одной линии
        elif (first_letter // 6 != second_letter // 6 and
                first_letter % 6 != second_letter % 6):
            # Проверка на то, что первая буква находится либо
            # слева и выше второй, либо
            # слева и ниже другой
            if(first_letter % 6 < second_letter % 6 and
            first_letter // 6 < second_letter // 6 or
            first_letter % 6 < second_letter % 6 and
            first_letter // 6 > second_letter // 6):
                # Запись сдвинутых значений
                result += str(key[first_letter
                            + abs(first_letter % 6
                            - second_letter % 6)])
                result += str(key[second_letter
                            - abs(second_letter % 6
                            - first_letter % 6)])
            # Проверка на то, что первая буква находится либо
            # справа и выше второй, либо
            # справа и ниже другой
            elif(first_letter % 6 > second_letter % 6 and
                first_letter // 6 < second_letter // 6 or
                first_letter % 6 > second_letter % 6 and
                first_letter // 6 > second_letter // 6):
                # Запись сдвинутых значений
                result += str(key[first_letter
                            - abs(first_letter % 6
                            - second_letter % 6)])
                result += str(key[second_letter
                            + abs(second_letter % 6
                            - first_letter % 6)])
        i += 2
    i = 0
    # Убирание буквы ф если в фразе идут две одинаковые буквы подряд
    while i < len(result):
        if i + 2 < len(result):
            if result[i] == result[i + 2] and result[i + 1] == 'ф':
                result = result[:i + 1] + result[i + 2:]
        elif i + 1 == len(result):
            result = result[:-1]
        i += 1
    result = unmark_upper_letter(result)
    result = result.replace("икразткая", "й")
    result = result.replace("ттыердвилоыкм", "ъ")
    return result


def main():
    print("Шифр Плэйфера")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    key = input("Введите ключ: ")
    key = convert(key)
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text, key))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text, key)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text, key)))
            print("Результат работы в файле new.txt")