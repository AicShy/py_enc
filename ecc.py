from ast import literal_eval
from conf import refactor_str_to_enc, refactor_str_from_enc, is_prime, coprime, str_to_numbers, numbers_to_str


# Функция проверки параметров
def check_params(p, k, q, Cb):
    if not is_prime(p):
        print('Неверное p, введите еще раз, оно должно быть простым')
        return False

    if k <= 0 or k >= q:
        print('Неверный k, введите еще раз, чтобы  0 < k < q')
        return False

    if Cb <= 0 or Cb >= q:
        print('Неверный Сb, введите еще раз, чтобы  0 < Сb < q')
        return False
    return True


# Функция проверки эллиптической кривой
def curve_check(a, b, p):
    if a < p:
        if (4 * (a ** 3) + 27 * (b ** 2)) % p != 0:
            return True
        else:
            print('Ваша кривая является особенной. Введите другие параметры')
            return False
    else:
        print('Введенный параметр а больше модуля p. Введите другие параметры')
        return False


# Функция подсчета фуннкци Эйлера
def euler_function(n):
    res = 0
    if is_prime(n):
        res = n - 1
    else:
        for i in range(n):
            if coprime(i,n):
                res += 1
    return res


# Функция умножения точек
def doubling_P(P, a, p):
    lam = ['', '']
    # Вычисление (3 * X1^2 + a) mod p
    lam[0] = (3 * P[0] ** 2 + a) % p
    # Вычисление (2 * Y1) mod p
    lam[1] = 2 * P[1] % p
    if lam[1] == 0:
        lam = 0
    # Если лямбда получилась не целым числом
    elif not (lam[0] / lam[1]).is_integer():
        lam[0] = lam[0] % p
        lam[1] = (lam[1] ** (euler_function(p) - 1)) % p
        # Вычисление лямбды
        lam = int(lam[0] * lam[1] % p)
    else:
        # Вычисление лямбды
        lam = int(lam[0] / lam[1])
    # Вычисление x-координаты полученной точки
    x = (lam ** 2 - 2 * P[0]) % p
    # Вычисление y-координаты полученной точки
    y = (lam * (P[0] - x) - P[1]) % p
    return x, y


# Функция сложения точек
def addition_P(P1, P2, p):
    lam = ['', '']
    # Вычисление (Y2 - Y1) mod p
    lam[0] = (P2[1] - P1[1]) % p
    # Вычисление (X2 - X1) mod p
    lam[1] = (P2[0] - P1[0]) % p
    if lam[1] == 0:
        lam = 0
    # Если лямбда получилась не целым числом
    elif not (lam[0] / lam[1]).is_integer():
        lam[0] = lam[0] % p
        lam[1] = (lam[1] ** (euler_function(p) - 1)) % p
        lam = int(lam[0] * lam[1] % p)
    else:
        # Вычисление лямбды
        lam = int(lam[0] / lam[1])
    # Вычисление x-координаты полученной точки
    x = (lam ** 2 - P1[0] - P2[0]) % p
    # Вычисление y-координаты полученной точки
    y = (lam * (P1[0] - x) - P1[1]) % p
    return x, y


# Функция генерации ключа
def key_gen(P, k, a, p):
    k = bin(k)[3:]
    Q = P
    for i in k:
        Q = doubling_P(Q, a, p)
        if i == '1':
            Q = addition_P(P, Q, p)
    return Q


# Функция шифрования
def encrypt(message, a, p, k, Cb, G):
    new_message = []
    for i in range(len(message)):
        # Публичный ключ, Db = [Cb]G
        public_key = key_gen(G, Cb, a, p)
        # Точка кривой R, R = [k]G
        private_key = key_gen(G, k, a, p)
        # Точка кривой P, P = [k]Db
        P = key_gen(public_key, k, a, p)
        new_message.append(private_key)
        # Получение e = mx mod p
        new_message.append(str(int(message[i]) * P[0] % p))
    return new_message


# Функция расшифрования
def decrypt(message, a, p, Cb):
    new_message = []
    for i in range(0, len(message), 2):
        # Вычисление Q = [Cb]R
        Q = key_gen(message[i], Cb, a, p)
        # Получение расшифрованного символа
        new_message.append(int(message[i+1]) * (Q[0] ** (p - 2)) % p)
    return new_message


def main():
    print("Шифр ECC – с использованием абсциссы точки")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите текст для шифрования: "))
            text = refactor_str_to_enc(text)
            text = str_to_numbers(text)
        while True:
            p = int(input("Введите простое p: "))
            q = int(input("Введите простое q: "))
            k = int(input("Введите k, при этом k > 0 и k < q: "))
            Cb = int(input("Введите Cb, при этом Cb > 0 и Cb <  q: "))
            if check_params(p, k, q, Cb):
                break
        while True:
            G = input("Введите G в формате [x, y]: ")
            try:
                G = literal_eval(G)
            except SyntaxError:
                print("Неверно введен G")
                continue
            if len(G) == 2:
                break
            else:
                print("Введено не 2 элемента")
        while True:
            a = int(input("Введите a, при этом a < p: "))
            b = int(input("Введите b: "))
            if  curve_check(a, b, p):
                break
        if big_or_small == 1:
            print("Шифрование: ", encrypt(text, a, p, k, Cb, G))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = str_to_numbers(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(encrypt(text, a, p, k, Cb, G)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = literal_eval(input("Введите текст для расшифрования: "))
        p = int(input("Введите простое p: "))
        a = int(input("Введите a, при этом a < p: "))
        Cb = int(input("Введите Cb: "))
        if big_or_small == 1:
            print("Расшифрование:", refactor_str_from_enc(numbers_to_str(decrypt(text, a, p, Cb))))
        if big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = literal_eval(file.read())
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(numbers_to_str(decrypt(text, a, p, Cb))))
            print("Результат работы в файле new.txt")
