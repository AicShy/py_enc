from ast import literal_eval
from conf import refactor_str_to_enc, is_prime

# Функция рассчета хэша
def generate_hash(text, n, q):
    h = [0]
    for char in text:
        h.append((h[-1] + (ord(char) - 1039))**2 % n)
    if h[-1] % q == 0:
        return 1
    return h[-1]


# Функция для рефакторинга строки перед выполнением программы и ее последующего хэширования
def convert_str_and_hash(text, n, q):
    text = refactor_str_to_enc(text)
    result = ""
    result = generate_hash(text, n, q)
    if result == 0:
        result = 1
    return result


# Функция проверки параметров
def check_params(p, q, a, x = 0):
    if not is_prime(p):
        print('Неверное p, введите еще раз, оно должно быть простым')
        return False
    if not is_prime(q) and (p - 1) % q != 0:
        print('Неверное q, введите еще раз, оно должно быть простым сомножителем p-1')
        return False
    if a <= 1 and a >= (p - 1) and (a ** q) % p != 1:
        print('Неверное а, введите еще раз, оно должно быть таким, что 1 < a < p-1 и (a^q) mod p = 1')
        return False
    if x >= q:
        print('Неверное x, введите еще раз, оно должно быть меньше q')
        return False
    return True


# Функция подписывания
def generate_signature(text, p, q, a, x, k, n):
    text = convert_str_and_hash(text, n, q)
    # Подсчет первого числа подписи
    # r = (а^k mod p) mod q
    r = ((a ** k) % p) % q
    if r == 0:
        print("Ошибка, r = 0, выберите другое k")
        return "Error"
    # Подсчет второго числа подписи
    # s = (х * r + k (Н(m))) mod q
    s = (x * r + k*text) % q
    y = (a ** x) % p
    print("Открытый ключ Y: ", y)
    r = r % 2**256
    s = s % 2**256
    return [r, s]


# Функция проверки подписи
def check_signature(text, r, s, p, q, a, y, n):
    text = convert_str_and_hash(text, n, q)
    # Проверка полученной подписи
    # v = Н(m)q-2 mod q,
    # z1 = (s * v) mod q,
    # z2 = ((q-r) * v) mod q,
    # u = ((а^z1 * у^z2 ) mod р) mod q.
    v = (text ** (q - 2)) % q
    z1 = (s * v) % q
    z2 = ((q - r) * v) % q
    u = ((a ** z1) * (y ** z2)) % p % q
    if u == r:
        res = 'Подпись верна'
    else:
        res = 'Подпись не верна'
    return res

def main():
    print("ЭЦП ГОСТ Р 34.10-94")
    what_to_do = int(input('Что делать? (подписать - 1, проверить подпись  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = str(input("Введите открытый текст: "))
        while True:
            p = int(input("Введите большое простое p: "))
            q = int(input("Введите простое q, являющееся сомножителем числа p-1: "))
            a = int(input("Введите такое а, что 1 < a < p-1 и (a^q) mod p = 1: "))
            x = int(input("Введите x < q: "))
            if  check_params(p, q, a, x):
                break
        while True:
            k = int(input("Введите случайное число k, при этом k < q: "))
            if  k >= q:
                print("Неверное k, введите еще раз, оно должно быть меньше q")
            else:
                break
        n = int(input("Введите модуль для хэширования n: "))
        if big_or_small == 1:
            print("Подпись: ", generate_signature(text, p, q, a, x, k, n))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(generate_signature(text, p, q, a, x, k, n)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input("Введите сообщение: ")
        while True:
            p = int(input("Введите большое простое p: "))
            q = int(input("Введите простое q, являющееся сомножителем числа p-1: "))
            a = int(input("Введите а: "))
            if  check_params(p, q, a):
                break
        y = int(input("Введите открытый ключ Y: "))
        n = int(input("Введите модуль для хэширования n: "))
        if big_or_small == 1:
            r = int(input("Введите r (первый параметр подписи): "))
            s = int(input("Введите s (второй параметр подписи): "))
            print(check_signature(text, r, s, p, q, a, y, n))
        if big_or_small == 2:
            text = open('test.txt', 'r',  encoding='UTF8').read()
            sign = literal_eval(open('output.txt', 'r',  encoding='UTF8').read())
            r = sign[0]
            s = sign[1]
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(check_signature(text, r, s, p, q, a, y, n))
            print("Результат работы в файле new.txt")
