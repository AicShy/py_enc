from conf import sBox, inverseSBox, Rcon, Rijndael_matrix, reversed_Rijndael_matrix, refactor_str_to_enc, refactor_str_from_enc, check_if_hex, str_to_hex, hex_to_str, add_to_text_itself, del_text_from_itself, list_of_elem_len_two


# Функция, переделывающая список элементов в одну строку
def str_of_hex(list):
    return ''.join(map(str, list))


# Функция для произведения XOR-a между колонками при вычислении раудовго ключа
def xor_columns(col1, col2):
    result = []
    for i in range(len(col1)):
        result.append((hex(int(col1[i], 16) ^ int(col2[i], 16)))[2:].zfill(2))
    return result


# Функция для изменения раудового ключа в конце каждой итерации
def change_round_key(key, iter, encryption):
    new_key = []
    if encryption:
        # берем последнюю колонку ключа
        column = key[12:]
        # переставляем первый HEX элемент на последнее место
        column = column[1:] + column[:1]
        # производим операцию sub_bytes
        column = sub_bytes(column, encryption)
        # производим XOR между полученной колонкой, первой колонкой ключа и колонкой Rcon, зависящей от номера раунда, получаем первую колонку нового ключа
        new_key += xor_columns(xor_columns(column, key[:4]), Rcon[iter])
        # производим XOR между соответствующей (1-3) полученной колонкой нового ключа и соответствующей + 1 (2-4) колонкой переданного ключа
        for i in range(3):
            new_key += xor_columns(new_key[i * 4: i * 4 + 4], key[(i+1) * 4: (i+1) * 4 + 4])
    else:
        i = 3
        tmp = []
        # производим XOR между соответствующей (3-1) колонкой ключа и колонкой -1 (2-0)
        while i >= 1:
            tmp += xor_columns(key[i * 4: i * 4 + 4], key[(i-1) * 4: (i-1) * 4 + 4])
            i -= 1
        # производим XOR между первой колонкой ключа и колонкой Rcon, зависящей от номера раунда
        column = xor_columns(key[:4], Rcon[iter])
        # берем первую колонку tmp
        some_p = tmp[:4]
        # переставляем первый HEX элемент на последнее место
        some_p = some_p[1:] + some_p[:1]
        # производим операцию sub_bytes
        some_p = sub_bytes(some_p, encryption=True)
        # производим XOR между column и some_p
        new_key = xor_columns(some_p, column)
        # меняем местами: 3 и 4 колонки, потом 2, потом 1
        new_key += tmp[8:] + tmp[4:8] + tmp[:4]
    return new_key


# Функция для XOR-сложения полученного раудового ключа и блока в обработке
def add_round_key(text, r_key):
    result = []
    for i in range(len(text)):
        result.append((hex(int(text[i], 16)^int(r_key[i], 16)))[2:].zfill(2))
    return result


# Функция, выполняющая операции SubBytes и InvSubBytes, замена производится по Sbox (S-блок AES)
def sub_bytes(text, encryption):
    result = []
    for i in text:
        if encryption:
            result.append(sBox[int(i[0], 16)][int(i[1], 16)])
        else:
            result.append(inverseSBox[int(i[0], 16)][int(i[1], 16)])
    return result


# Функция для конвертации одномерного массива в двумерную матрицу
def convert_to_matrix(arr):
    result = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    for i in range(len(arr)):
        result[i%4][i//4] = arr[i]
    return result


# Функция для конвертации двумерной матрицы в одномерный массив
def convert_to_list(matrix):
    result = []
    for i in range(4):
        for j in range(4):
            result.append(matrix[j][i])
    return result


# Функция, выполняющая операции ShiftRows и InvShiftRows
def shift_rows(text, encryption):
    text = convert_to_matrix(text)
    for i in range(1,4):
        # В процедуре ShiftRows байты в каждой строке циклически сдвигаются влево. Размер смещения байтов каждой строки зависит от её номера (0 - 0, 1 - 1 и т. д.)
        if encryption:
            text[i] = text[i][i:] + text[i][:i]
        # В процедуре InvShiftRows байты в каждой строке циклически сдвигаются вправо. Размер смещения байтов каждой строки зависит от её номера (0 - 0, 1 - 1 и т. д.)
        else:
            text[i] = text[i][-i:] + text[i][:-i]
    text = convert_to_list(text)
    return text


# Функция, выполняющая умножение числа из колонки на 2
def mul_by_2(num):
    # если первый бит последовательности - 1
    if int(num, 2) >= 128:
        num = num[1:] + '0'
        num = bin(int(num, 2) ^ int("1b", 16))[2:].zfill(8)
    else:
        num = num[1:] + '0'
    return num


# Функция, выполняющая операцию MixColumns и InvMixColumns
def mix_columns(column, row, encryption):
    result = 0
    if encryption:
        for i in range(4):
            bin_num = bin(int(column[i], 16))[2:].zfill(8)
            if Rijndael_matrix[row][i] == 2:
                bin_num = mul_by_2(bin_num)
            elif Rijndael_matrix[row][i] == 3:
                init_bin_num = int(bin_num, 2)
                bin_num = bin(int(mul_by_2(bin_num), 2) ^ init_bin_num)[2:].zfill(8)
            if i == 0:
                result = int(bin_num, 2)
            else:
                result ^= int(bin_num, 2)
    else:
        for i in range(4):
            bin_num = bin(int(column[i], 16))[2:].zfill(8)
            init_bin_num = int(bin_num, 2)
            if reversed_Rijndael_matrix[row][i] == 9:
                # 09=(((x·2)·2)·2)+x
                bin_num = int(mul_by_2(mul_by_2(mul_by_2(bin_num))), 2) ^ init_bin_num
            elif reversed_Rijndael_matrix[row][i] == 11:
                # 11=((((x·2)·2)+x)·2)+x
                bin_num = int(mul_by_2(bin(int(mul_by_2(mul_by_2(bin_num)), 2) ^ init_bin_num)[2:].zfill(8)), 2) ^ init_bin_num
            elif reversed_Rijndael_matrix[row][i] == 13:
                # 13=((((x·2)+x)·2)·2)+x
                bin_num = int(mul_by_2(mul_by_2(bin(int(mul_by_2(bin_num), 2) ^ init_bin_num)[2:].zfill(8))), 2) ^ init_bin_num
            elif reversed_Rijndael_matrix[row][i] == 14:
                # 14=((((x·2)+x)·2)+x)·2
                bin_num = int(mul_by_2(bin(int(mul_by_2(bin(int(mul_by_2(bin_num), 2) ^ init_bin_num)[2:].zfill(8)), 2) ^ init_bin_num)[2:].zfill(8)), 2)
            if i == 0:
                result = bin_num
            else:
                result ^= bin_num
    return hex(result)[2:].zfill(2)


# Функция шифрования
def encrypt(text, key):
    res = []
    init_key = key
    for i in range(len(text) // 16):
        encryption = True
        # AddRoundKey
        tmp = add_round_key(text[i * 16 : i * 16 + 16], key)
        # Начало 9 раундов
        for j in range(9):
            result = []
            # SubBytes
            tmp = sub_bytes(tmp, encryption)
            # ShiftRows
            tmp = shift_rows(tmp, encryption)
            # MixColumns
            for l in range(4):
                column = tmp[l * 4: l * 4 + 4]
                for k in range(4):
                    result.append(mix_columns(column, k, encryption))
            # AddRoundKey
            key = change_round_key(key, j, encryption)
            tmp = add_round_key(result, key)
        # Конец 9 раундов
        # SubBytes
        tmp = sub_bytes(tmp, encryption)
        # ShiftRows
        tmp = shift_rows(tmp, encryption)
        # AddRoundKey
        key = change_round_key(key, 9, encryption)
        res.extend(add_round_key(tmp, key))
        key = init_key
    return res



# Функция расшифрования
def decrypt(text, key):
    res = []
    i = 0
    # получение конечного ключа
    while i <= 9:
        key = change_round_key(key, i, True)
        i += 1
    init_key = key
    encryption = False
    for f in range(len(text) // 16):
        # AddRoundKey
        tmp = add_round_key(text[f * 16 : f * 16 + 16], key)
        # InvShiftRows
        tmp = shift_rows(tmp, encryption)
        # InvSubBytes
        tmp = sub_bytes(tmp, encryption)
        # Изменить раундовый ключ
        key = change_round_key(key, 9, encryption)
        i = 8
        while i >= 0:
            result = []
            # AddRoundKey
            tmp = add_round_key(tmp, key)
            # Изменить раундовый ключ
            key = change_round_key(key, i, encryption)
            for j in range(4):
                column = tmp[j * 4: j * 4 + 4]
                for k in range(4):
                    # InvMixColumns
                    result.append(mix_columns(column, k, encryption))
            tmp = result
            # InvShiftRows
            tmp = shift_rows(tmp, encryption)
            # InvSubBytes
            tmp = sub_bytes(tmp, encryption)
            i -= 1
        res.extend(add_round_key(tmp, key))
        key = init_key
    return res


# Функция main, ввод данных, вызов других функций
def main():
    print("Шифр AES")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if big_or_small == 1 and what_to_do == 1:
        hex_flag = int(input('Формат входных данных? (hex - 1, str  - 2): '))
    if big_or_small == 1 and what_to_do == 2:
        hex_flag = int(input('Формат выходных данных? (hex - 1, str  - 2): '))
    while True:
        key = input("Введите ключ в формате hex длиной 32 символов: ").upper()
        if check_if_hex(key):
            if len(key) == 32:
                key = list_of_elem_len_two(key)
                break
            else:
                print("Длина ключа не 32 символа")
    if what_to_do == 1:
        if big_or_small == 1:
            while True:
                text = str(input("Введите строку для шифрования в формате, указанном ранее: "))
                if hex_flag == 1:
                    text = text.upper()
                    if check_if_hex(text):
                        text = add_to_text_itself(text, 32)
                        text = list_of_elem_len_two(text)
                        break
                elif hex_flag == 2:
                    text = refactor_str_to_enc(text)
                    text = add_to_text_itself(text, 16)
                    text = str_to_hex(text)
                    break
            print("Шифрование:", str_of_hex(encrypt(text, key)), sep="\n")
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = add_to_text_itself(text, 16)
            text = str_to_hex(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(str_of_hex(encrypt(text, key))))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            while True:
                text = str(input("Введите строку для расшифрования в формате hex: "))
                text = text.upper()
                if check_if_hex(text):
                    text = list_of_elem_len_two(text)
                    break
            if hex_flag == 1:
                print("Расшифрование:", str_of_hex(decrypt(text, key)), sep="\n")
            elif hex_flag == 2:
                print("Расшифрование:",  refactor_str_from_enc(del_text_from_itself(hex_to_str(decrypt(text, key)))), sep="\n")
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            f = open('new.txt', 'w',  encoding='UTF8')
            text = list_of_elem_len_two(file.read())
            f.write(str(refactor_str_from_enc(del_text_from_itself(hex_to_str(decrypt(text, key))))))
            print("Результат работы в файле output.txt")