from math import ceil
from collections import Counter
from conf import refactor_str_from_enc, refactor_str_to_enc

# Функция для конвертации ключа, для использования слова с повторяющимеся буквами в качестве ключа
def convert_key(key):
    new_key = []
    count_elem = dict(Counter(key))
    letters_add = dict((i, 0) for i in key)
    # В случае, если буква в ключе импользуется 1 раз, заменить значение на -1
    for i in key:
        if count_elem[i] == 1:
            count_elem[i] = -1
    # Добавление порядковых номеров к повторяющимся буквам
    for i in key:
        if count_elem[i] != -1:
            letters_add[i] += 1
            new_key.append(i + str(letters_add[i]))
            count_elem[i] -= 1
        else:
            new_key.append(i)
    return new_key


def encrypt(text, key):
    result = ''
    text = list(text)
    len_key = len(key)
    tmp_res = []
    # Конвертация ключа для использования слова с повторяющимеся буквами в качестве ключа
    key = convert_key(key)
    # Начало формирования таблицы шифрования, добавление в таблицу ключа
    for i in key:
        tmp_res.append(i)
    i = 0
    # Добавление пустых символов для правильного шифрования
    while len(text) % len_key != 0:
        text.append('')
    # Формирование остальной части таблицы шифрования (запись фразы "змейкой")
    for i in range(len(text) // len_key):
        if i % 2 == 0:
            tmp_res.extend(text[i * len_key : i * len_key + len_key])
        else:
            tmp_res.extend(text[i * len_key : i * len_key + len_key][::-1])
    # Сортирование символов в ключе в алфавитном порядке
    new_key = sorted(key)
    for i in new_key:
        # Запись шифрования столбцами с избавлением от пустых символов,
        # добавленных ранее, путем конвертации списка в строку
        result += ''.join(str(e) for e in tmp_res[tmp_res.index(i) :: len_key][1::])
    return result


def decrypt(text, key):
    text = list(text)
    len_key = len(key)
    tmp_res = []
    key = list(key)
    # Конвертация ключа для использования слова с повторяющимеся буквами в качестве ключа
    added_numbers_key = convert_key(key)
    new_key = sorted(added_numbers_key)
    key = added_numbers_key
    # Обработка случая, когда длина сообщения не вписывается равно в таблицу, т.е.
    # добавление пробелов в нужные места в шифртекст
    if len(text) % len_key != 0:
        # То, сколько должно получиться строчек
        ceiled_division = ceil(len(text) / len_key)
        # То, сколько пробелов надо добавить
        must_add_this = len_key - len(text) % len_key
        # Если количество строк четное
        i = 0
        if ceiled_division % 2 == 0:
            # Те буквы ключа, под которыми будут пробелы
            slice_sotrted_key = sorted(key[:must_add_this])
        # Если количество строк нечетное
        else:
            # Те буквы ключа, под которыми будут пробелы
            slice_sotrted_key = sorted(key[len_key - must_add_this:])
        # Добавление пробела в фишртекст
        while must_add_this > 0:
            text.insert(new_key.index(slice_sotrted_key[i]) * ceiled_division
                        + len(text) // len_key, '')
            i += 1
            must_add_this -= 1
    # Формирование начальной шифртаблицы
    for i in key:
        tmp_res.extend(text[new_key.index(i) * (len(text) // len_key) : new_key.index(i) * (len(text) // len_key) + (len(text) // len_key)])
    i = 0
    result = []
    # Запись расшифрованного результата из шифртаблицы
    while i < (len(text) // len_key):
        if i % 2 == 0:
            result.extend(tmp_res[i::len(text) // len_key])
        else:
            result.extend(tmp_res[i::len(text) // len_key][::-1])
        i += 1
    return ''.join(str(e) for e in list(result))


def main():
    print("Шифр Вертикальная перестановка")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    key = input("Введите ключ: ")
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text, key))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text, key)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text, key)))
            print("Результат работы в файле new.txt")
