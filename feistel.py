import magma_s_block
from textwrap import wrap
from conf import check_if_hex, refactor_str_to_enc, refactor_str_from_enc, str_to_hex, hex_to_str, add_to_text_itself, del_text_from_itself, list_of_elem_len_two


# Функция F
def f(side, key_chunk):
    # Сумма по модулю 2 в 32
    res = hex((int(side, 16) + int(key_chunk, 16)) % (2**32))[2:].zfill(8)
    # S-блок
    res = magma_s_block.encrypt(res)
    res = bin(int(res, 16))[2:].zfill(32)
    # Сдвиг влево на 11
    res = hex(int(res[11:] + res[:11], 2))[2:]
    return res


def encrypt(text, key):
    result = ''
    key = key[:64]
    new_key = []
    enc = {}
    # блоки по 64 бита
    # len(Li) = 32 бита, len(Ri) = 32 бита,
    key = wrap(key, 8)
    for i in range(3):
        new_key.extend(key)
    new_key.extend(key[::-1])
    for i in range(len(text) // 16):
        # Запись первой левой части
        enc['L0'] = text[i * 16 : i * 16 + 8]
        # Запись первой правой части
        enc['R0'] = text[i * 16 + 8 : i * 16 + 16]
        for j in range(32):
            # Смена новой левой части на правую прошлой итерации
            enc["L" + str(j + 1)] = enc['R'+ str(j)]
            # Смена правой части на XOR между прошлой левой частью и значение функции F от прошлой правой части и ключа
            enc["R" + str(j + 1)] = hex(int(enc['L' + str(j)], 16) ^ int(f(enc['R'+ str(j)], new_key[j]), 16))[2:]
        # print(enc["R32"], enc["R31"])
        result += enc["R32"].zfill(8) + enc["R31"].zfill(8)
    return result


def decrypt(text, key):
    result = ''
    key = key[:64]
    new_key = []
    enc = {}
    # блоки по 64 бита
    # len(Li) = 32 бита, len(Ri) = 32 бита,
    key = wrap(key, 8)
    for i in range(3):
        new_key.extend(key)
    new_key.extend(key[::-1])
    new_key = new_key[::-1]
    i = 0
    for i in range(len(text) // 16):
        # Запись первой левой части
        enc['L0'] = text[i * 16 : i * 16 + 8]
        # Запись первой правой части
        enc['R0'] = text[i * 16 + 8 : i * 16 + 16]
        for j in range(32):
            # Смена новой левой части на правую прошлой итерации
            enc["L" + str(j + 1)] = enc['R'+ str(j)]
            # Смена правой части на XOR между прошлой левой частью и значение функции F от прошлой правой части и ключа
            enc["R" + str(j + 1)] = hex(int(enc['L' + str(j)], 16) ^ int(f(enc['R'+ str(j)], new_key[j]), 16))[2:]
        result += enc["R32"].zfill(8) + enc["R31"].zfill(8)
    return result


def main():
    print("Перестановка в комбинационных шифрах - сеть Фейстеля")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if big_or_small == 1 and what_to_do == 1:
        hex_flag = int(input('Формат входных данных? (hex - 1, str  - 2): '))
    if big_or_small == 1 and what_to_do == 2:
        hex_flag = int(input('Формат выходных данных? (hex - 1, str  - 2): '))
    while True:
        key = input("Введите ключ в формате hex длиной 64: ").upper()
        if check_if_hex(key):
            if len(key) == 64:
                break
            else:
                print("Длина ключа должна быть 64")
    if what_to_do == 1:
        if big_or_small == 1:
            while True:
                if hex_flag == 1:
                    text = input('Введите строку в формате hex: ').upper()
                    if check_if_hex(text):
                        text = add_to_text_itself(text, 16)
                        break
                elif hex_flag == 2:
                    text = input('Введите строку: ')
                    text = refactor_str_to_enc(text)
                    text = add_to_text_itself(text, 8)
                    text = str_to_hex(text)
                    text = "".join(text)
                    break
            print("Шифрование: ", (encrypt(text, key)))
        if big_or_small == 2:
            file = open("test.txt", "r", encoding='utf-8')
            text = refactor_str_to_enc(file.read())
            text = add_to_text_itself(text, 8)
            text = str_to_hex(text)
            text = "".join(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(str(encrypt(text, key)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            while True:
                text = input('Введите строку в формате hex: ').upper()
                if check_if_hex(text):
                    break
            if hex_flag == 1:
                print("Расшифрование:", del_text_from_itself(decrypt(text, key)))
            elif hex_flag == 2:
                print("Расшифрование: ", refactor_str_from_enc(del_text_from_itself(hex_to_str(list_of_elem_len_two(decrypt(text, key))))))
        if big_or_small == 2:
            file = open("output.txt", "r", encoding='utf-8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(str(refactor_str_from_enc(del_text_from_itself(hex_to_str(list_of_elem_len_two(decrypt(text, key)))))))
            print("Результат работы в файле new.txt")
