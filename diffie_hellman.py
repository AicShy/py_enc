# Функция для генерации открытого ключа для пользователя
def open_key_gen(n, a, Ka):
    Ya = a**Ka % n
    return Ya


# Функция для генерации общего секретного ключа, используя параметры одного пользователя
def secret_key_gen(n, Ka, Yb):
    K_general = (Yb ** Ka) % n
    return K_general


# Функция проверки генерации общего секретного ключа
def secret_key_check(n, a, Ka, Kb):
    Ya = open_key_gen(n, a, Ka)
    Yb = open_key_gen(n, a, Kb)
    K_1 = secret_key_gen(n, Ka, Yb)
    K_2 = secret_key_gen(n, Kb, Ya)
    if K_1 == K_2:
        return f'Общий ключ: {K_1}'
    else:
        return f'Ошибка!\nK_1: {K_1}\nK_2: {K_2}'

def main():
    print("Обмен ключами по Диффи-Хеллману")
    what_to_do = int(input('Что делать? (получить свой открытый ключ - 1, получить общий закрытый ключ  - 2, проверка общего ключа - 3)\n'))
    while True:
        n = int(input("Введите n, при этом n > 1: "))
        if n <= 1:
            print('Число n должно соответствовать условию 1 < n')
        else:
            break
    while True:
        a = int(input("Введите a, при этом 1 < a < n: "))
        if a >= n or a <= 1:
            print('Число a должно соответствовать условию 1 < a < n')
        else:
            break
    if what_to_do == 1:
        while True:
            Ka = int(input("Введите Ka, при этом 2 < Ka < n - 1: "))
            if Ka >= n-1 or Ka <= 2:
                print('Число Ka должно соответствовать условию 2 < Ka < n-1')
            else:
                break
        print(f'Открытый ключ: {open_key_gen(n, a, Ka)}')
    elif what_to_do == 2:
        while True:
            Ka = int(input("Введите Ka, при этом 2 < Ka < n - 1: "))
            if Ka >= n-1 or Ka <= 2:
                print('Число Ka должно соответствовать условию 2 < Ka < n-1')
            else:
                break
        Yb = int(input("Введите Yb (открытый ключ другого пользователя): "))
        print(f'Общий секретный ключ: {secret_key_gen(n, Ka, Yb)}')
    elif what_to_do == 3:
        while True:
            Ka = int(input("Введите Ka, при этом 2 < Ka < n - 1: "))
            if Ka >= n-1 or Ka <= 2:
                print('Число Ka должно соответствовать условию 2 < Ka < n-1')
            else:
                break
        while True:
            Kb = int(input("Введите Kb (секретный ключ другого пользователя), при этом 2 < Kb < n - 1: "))
            if Kb >= n-1 or Kb <= 2:
                print('Число Kb должно соответствовать условию 2 < Ka < n-1')
            else:
                break
        print(secret_key_check(n, a, Ka, Kb))
