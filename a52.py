from conf import str_to_bin, bin_to_str, refactor_str_to_enc, refactor_str_from_enc, check_if_bin


# Функция поиска мажоритарного бита
def maj(b1, b2, b3):
    b1 = int(b1)
    b2 = int(b2)
    b3 = int(b3)
    return int((b1 and b2) or (b1 and b3) or (b2 and b3))


# Функция, которая принимает на вход введенные пользователем ключ и длину гаммы
# и возвращает получившуюся гамму
def make_and_get_gamma(s_key, gamma_len):
    R1 = s_key[:19]
    R2 = s_key[19:41]
    R3 = s_key[41:64]
    R4 = s_key[64:81]
    gamma = ""
    for i in range(gamma_len):
        # print(i, "гамма: ", str(int(R1[-1]) ^ int(R2[-1]) ^ int(R3[-1]) ^ maj(R1[15], R1[14], R1[12]) ^ maj(R2[16], R2[13], R2[9]) ^ maj(R3[18], R3[16], R3[13])))
        S1 = int(R4[3])
        S2 = int(R4[7])
        S3 = int(R4[10])
        gamma += str(int(R1[-1]) ^ int(R2[-1]) ^ int(R3[-1]) ^ maj(R1[15], R1[14], R1[12]) ^ maj(R2[16], R2[13], R2[9]) ^ maj(R3[18], R3[16], R3[13]))
        f_function = maj(S1, S2, S3)
        if S1 == f_function:
            # получение нового бита происходит путем применения операции XOR к соответствующим битам регистра
            new_bit = int(R2[21]) ^ int(R2[20])
            R2 = str(new_bit) + R2[:21]
        if S2 == f_function:
            # получение нового бита происходит путем применения операции XOR к соответствующим битам регистра
            new_bit = int(R3[22]) ^ int(R3[21]) ^ int(R3[20]) ^ int(R3[7])
            R3 = str(new_bit) + R3[:22]
        if S3 == f_function:
            # получение нового бита происходит путем применения операции XOR к соответствующим битам регистра
            new_bit = int(R1[18]) ^ int(R1[17]) ^ int(R1[16]) ^ int(R1[13])
            R1 = str(new_bit) + R1[:18]
        R4 = str(int(R4[16]) ^ int(R4[11])) + R4[:16]
    return gamma


def encrypt(gamma, text):
    result = ""
    for i in range(len(text)):
        result += str(int(text[i]) ^ int(gamma[i]))
    return result


# Функция main, ввод данных, вызов других функций
def main():
    print("Шифр А5/2")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if big_or_small == 1 and what_to_do == 1:
        bin_flag = int(input('Формат входных данных? (bin - 1, str  - 2): '))
    if big_or_small == 1 and what_to_do == 2:
        bin_flag = int(input('Формат выходных данных? (bin - 1, str  - 2): '))
    while True:
        key = input("Введите ключ в формате bin длиной 81 символов: ").upper()
        if check_if_bin(key):
            if len(key) == 81:
                break
            else:
                print("Длина ключа не 81 символов")
    if what_to_do == 1:
        if big_or_small == 1:
            while True:
                text = str(input("Введите строку для шифрования в формате, указанном ранее: "))
                if bin_flag == 1:
                    text = text.upper()
                    if check_if_bin(text):
                        break
                elif bin_flag == 2:
                    text = refactor_str_to_enc(text)
                    text = str_to_bin(text)
                    break
            gamma = make_and_get_gamma(key, len(text))
            print("Шифрование: ", encrypt(gamma, text))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = str_to_bin(text)
            f = open('output.txt', 'w',  encoding='UTF8')
            gamma = make_and_get_gamma(key, len(text))
            f.write(str(encrypt(gamma, text)))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            while True:
                text = str(input("Введите строку в формате bin для расшифрования: "))
                if check_if_bin(text):
                    break
            gamma = make_and_get_gamma(key, len((text)))
            if bin_flag == 1:
                print("Расшифрование:", (encrypt(gamma, text)))
            elif bin_flag == 2:
                print("Расшифрование: ", refactor_str_from_enc(bin_to_str(encrypt(gamma, text))))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            gamma = make_and_get_gamma(key, len(text))
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(bin_to_str(encrypt(gamma, text))))
            print("Результат работы в файле new.txt")