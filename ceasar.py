from conf import refactor_str_from_enc, refactor_str_to_enc


# Функция шифрования
def encrypt(text, key):
    result = ""
    key = int(key)
    for i in text:
        if i.isupper():
            result += chr(((ord(i) + key - 1040)) % 32 + 1040)
        else:
            result += chr(((ord(i) + key - 1072)) % 32 + 1072)
    return result


# Функция расшифрования
def decrypt(text, key):
    result = ""
    key = int(key)
    for i in text:
        if i.isupper():
            result += chr(((ord(i) - key - 1040)) % 32 + 1040)
        else:
            result += chr(((ord(i) - key - 1072)) % 32 + 1072)
    return result


def main():
    print("Шифр Цезарь")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    key = input("Введите ключ: ")
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text, key))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text, key)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text, key)))
            print("Результат работы в файле new.txt")

# whatToDo = input('Что делать? (шифровать - 1, расшифровать  - 2)\n')
# if whatToDo == '1':
#     text = input('Введите текст для шифрования\n')
#     key = input('Введите ключ\n')
#     print('При шифровке получилось следующее ', encrypt(text, key))
# elif whatToDo == '2':
#     text = input('Введите текст для расшифровки\n')
#     key = input('Введите ключ\n')
#     print('При шифровке получилось следующее ', decrypt(text, key))
# else:
#     print("Неверный ввод")
