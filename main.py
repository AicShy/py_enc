import a51, a52, aes, atbash, belazo, cardano, ceasar, diffie_hellman, ecc_ds,\
        ecc, elgamal_ds, elgamal, feistel, gost_r_34_10_94,\
        gost_r_34_13_2015_gamma, magma_s_block, matrix, playfair, polibiya,\
        rsa_ds, rsa, shennon, tritimie, vertikal, viziner
while True:
    chip_num = int(input('''Выберите нужный вариант:
                1 - Шифр АТБАШ
                2 - Шифр Цезаря
                3 - Квадрат Полибия
                4 - Шифр Тритемия
                5 - Шифр Белазо
                6 - Шифр Виженера
                7 - S-блок замены ГОСТ Р 34.12-2015 («МАГМА»)
                8 - Матричный шифр (матрица-ключ не меньше 3х3)
                9 - Шифр Плэйфера – шифр биграммной замены
                10 - Вертикальная перестановка
                11 - Решетка Кардано
                12 - Перестановка в комбинационных шифрах (DES, ГОСТ 28147-89, МАГМА) - сеть Фейстеля
                13 - Одноразовый блокнот К.Шеннона
                14 - ГОСТ Р 34.12-2015 «Магма» (гаммирование ГОСТ Р 34.13-2015)
                15 - А5 /1
                16 - А5 /2
                17 - ГОСТ Р 34.12-2015 «Магма» (гаммирование ГОСТ Р 34.13-2015)
                19 - AES
                21 - RSA
                22 - Elgamal
                23 - ECC – С ИСПОЛЬЗОВАНИЕМ АБСЦИССЫ ТОЧКИ
                24 - ЭЦП RSA
                25 - ЭЦП Elgamal
                26 - ЭЦП ГОСТ Р 34.10-94
                27 - ГОСТ Р 34.10-2012
                28 - Обмен ключами по алгоритму Diffie–Hellman
                29 - Выход
                Выбранный вариант: '''))

    if chip_num == 1:
        atbash.main()
    elif chip_num == 2:
        ceasar.main()
    elif chip_num == 3:
        polibiya.main()
    elif chip_num == 4:
        tritimie.main()
    elif chip_num == 5:
        belazo.main()
    elif chip_num == 6:
        viziner.main()
    elif chip_num == 7:
        magma_s_block.main()
    elif chip_num == 8:
        matrix.main()
    elif chip_num == 9:
        playfair.main()
    elif chip_num == 10:
        vertikal.main()
    elif chip_num == 11:
        cardano.main()
    elif chip_num == 12:
        feistel.main()
    elif chip_num == 13:
        shennon.main()
    elif chip_num == 14:
        gost_r_34_13_2015_gamma.main()
    elif chip_num == 15:
        a51.main()
    elif chip_num == 16:
        a52.main()
    elif chip_num == 17:
        gost_r_34_13_2015_gamma.main()
    elif chip_num == 19:
        aes.main()
    elif chip_num == 21:
        rsa.main()
    elif chip_num == 22:
        elgamal.main()
    elif chip_num == 23:
        ecc.main()
    elif chip_num == 24:
        rsa_ds.main()
    elif chip_num == 25:
        elgamal_ds.main()
    elif chip_num == 26:
        gost_r_34_10_94.main()
    elif chip_num == 27:
        ecc_ds.main()
    elif chip_num == 28:
        diffie_hellman.main()
    elif chip_num == 29:
        exit()
    print()
    if_exit = int(input("Продолжить работу? (1 - да, 0 - нет): "))
    if not if_exit:
        exit()