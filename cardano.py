from conf import refactor_str_from_enc, refactor_str_to_enc, add_to_text_itself, del_text_from_itself


# Решетка
key = ([0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 1, 0, 1, 1, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
        0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 1, 0, 0, 1 ])

# Ширина решетки
m = 10


# Высота решетки
k = 6

# Функция для поворота решетки
def rotate_grille(key, j):
    # поворот осуществляется после записи шифротекста
    # после первой и третьей итерации повершуть решетку на 90 градусов
    if j == 0 or j == 2:
        key = key[::-1]
    # после второй итерации перевернуть решетку
    if j == 1:
        new_key = []
        for l in range(k):
            new_key.extend(key[l * m : l * m + m][::-1])
        key = new_key
    return key



# Функция шифрования
def encrypt(text, key, m ,k):
    text = list(text)
    len_of_block = m * k
    len_text = len(text)
    new_text = []
    # Генерация матрицы для записи результата
    result = {x: "" for x in range(len(text))}
    # Перезапись текста в двумерный массив, длина одного элемента - размер одной решетки
    for i in range(len_text // len_of_block):
        new_text.append(text[i * len_of_block : i * len_of_block + len_of_block])
    # Начало накладывания решетки
    for i in range(len(new_text)):
        counter = 0
        for j in range(4):
            for h in range(len_of_block):
                if key[h] == 1:
                    # Запись текста по решетке
                    result[i * len_of_block + h] = new_text[i][counter]
                    counter += 1
            # Поворот решетки
            key = rotate_grille(key, j)
    return ''.join(str(e) for e in list(result.values()))


def decrypt(text, key, m ,k):
    text = list(text)
    len_of_block = m * k
    len_text = len(text)
    new_text = []
    # Генерация матрицы для записи результата
    result = {x: "" for x in range(len(text))}
    # Перезапись шифротекста в двумерный массив, длина одного элемента - размер одной решетки
    for i in range(len_text // len_of_block):
        new_text.append(text[i * len_of_block : i * len_of_block + len_of_block])
    # Начало накладывания решетки
    for i in range(len(new_text)):
        counter = 0
        for j in range(4):
            for h in range(len_of_block):
                if(key[h] == 1):
                    # Запись текста по решетке
                    result[i * len_of_block + counter] = new_text[i][h]
                    counter += 1
            # Поворот решетки
            key = rotate_grille(key, j)
    return ''.join(str(e) for e in list(result.values()))


def main():
    print("Шифр Решетка Кардано")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            text = add_to_text_itself(text, m * k)
            print("Шифрование: ", encrypt(text, key, m, k))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = add_to_text_itself(text, m * k)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key, m, k))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(del_text_from_itself(decrypt(text, key, m, k))))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(del_text_from_itself(decrypt(text, key, m, k))))
            print("Результат работы в файле new.txt")
