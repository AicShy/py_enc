from conf import refactor_str_from_enc, refactor_str_to_enc, mark_upper_letter, unmark_upper_letter


alphabet = {'а': 0, 'б': 1, 'в': 2, 'г': 3, 'д': 4,
'е': 5, 'ж': 6, 'з': 7, 'и': 8, 'й': 9,
'к': 10, 'л': 11, 'м': 12, 'н': 13, 'о': 14,
'п': 15, 'р': 16, 'с': 17, 'т': 18, 'у': 19,
'ф': 20, 'х': 21, 'ц': 22, 'ч': 23, 'ш': 24,
'щ': 25, 'ъ': 26, 'ы': 27, 'ь': 28, 'э': 29,
'ю': 30, 'я': 31}


reversed_alphabet = {0: 'а', 1: 'б', 2: 'в', 3: 'г',
4: 'д', 5: 'е', 6: 'ж', 7: 'з', 8: 'и', 9: 'й',
10: 'к', 11: 'л', 12: 'м', 13: 'н', 14: 'о',
15: 'п', 16: 'р', 17: 'с', 18: 'т', 19: 'у',
20: 'ф', 21: 'х', 22: 'ц', 23: 'ч', 24: 'ш',
25: 'щ', 26: 'ъ', 27: 'ы', 28: 'ь', 29: 'э',
30: 'ю', 31: 'я'}


m = 32

# Функция шифрования
def encrypt(text, a, c, Ti):
    result = ''
    gamma = []
    text = mark_upper_letter(text)
    for letter in text:
        number = alphabet[letter]
        # Вычисляем алфавитный номер буквы шифртекста
        number_encr = (number + Ti) % m
        result += reversed_alphabet[number_encr]
        # Линейный генератор:
        # Т(i+1) = (aT(i) + с) mod m, где а и с – константы, Т(j) – исходная величина,
        # выбранная в качестве порождающего числа. Период ограничен модулем и не может быть больше m
        Ti = (a * Ti + c) % m
        gamma.append(Ti)
    # print("Гамма: ", gamma)
    return result


def decrypt(text, a, c, Ti):
    result = ""
    gamma = []
    for letter in text:
        number = alphabet[letter]
        # Вычисляем алфавитный номер буквы открытого текста
        number_decr = (number - Ti) % m
        result += reversed_alphabet[number_decr]
        # Линейный генератор:
        # Т(i+1) = (aT(i) + с) mod m, где а и с – константы, Т(j) – исходная величина,
        # выбранная в качестве порождающего числа. Период ограничен модулем и не может быть больше m
        Ti = (a * Ti + c) % m
        gamma.append(Ti)
    result = unmark_upper_letter(result)
    return result


def main():
    print("Шифр Одноразовый блокнот К.Шеннона")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    a = int(input("Введите a: "))
    c = int(input("Введите c: "))
    Ti = int(input("Введите T0: "))
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            print("Шифрование: ", encrypt(text, a, c, Ti))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, a, c, Ti))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(decrypt(text, a, c, Ti)))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(decrypt(text, a, c, Ti)))
            print("Результат работы в файле new.txt")