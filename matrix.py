import numpy as np
from math import sqrt
from conf import refactor_str_from_enc, refactor_str_to_enc, add_to_text_itself, del_text_from_itself, str_to_numbers, numbers_to_str

# Функция шифрования
def encrypt(text, key, size):
    result = []
    new_text = []
    # создание матрицы из текста
    for i in range(len(text) // size):
        new_text = list(text[i * size : i * size + size])
        # перевод полученных букв в цифры
        for j in range(size):
            new_text[j] = int(str_to_numbers(new_text[j])[0])
        # умножение матрицы-ключа на полученную матрицу-текстовую
        result.append(list(np.dot(key, new_text)))
    return ','.join(map(str,[ y for x in result for y in x]))

# Функция расшифрования
def decrypt(text, key, size):
    result = []
    new_text = []
    text = text.split(',')
    # вычисление обратной матрицы
    reversed_key = (np.linalg.inv(key))
    # создание матрицы из текста
    for i in range(len(text) // size):
        new_text = list(map(int, (text[i * size : i * size + size])))
        # умножение обратной матрицы-ключа на полученную матрицу-шифротекстовую
        matr = list(np.dot(reversed_key, new_text))
        for j in range(size):
            result.append(round(matr[j]))
    result = numbers_to_str(result)
    return result


def main():
    print("Шифр Матричный")
    what_to_do = int(input('Что делать? (шифровать - 1, расшифровать  - 2): '))
    big_or_small = int(input('Ручной ввод строки или из файла? (ручной - 1, из файла  - 2): '))
    while True:
        while True:
            size = int(input("Введите размер матрицы-ключа (один символ): "))
            if size >= 3:
                break
            else:
                print("Размер матрицы должен быть больше 3")
        key = list(map(int, input("Введите матрицу-ключ в строку через пробел:\n").split()))
        if round(sqrt(len(key))) == size:
            new_key = []
            for i in range(size):
                new_key.append(key[i * size : i * size + size])
            # Проверка ключа-матрицы на обратимость
            if np.linalg.det(new_key) != 0:
                key = new_key
                break
            else:
                print("Ошибка, ключ-матрица не обратима")
        else:
            print("Размер введенной матрицы не соответствует переданному размеру")
    if what_to_do == 1:
        if big_or_small == 1:
            text = input('Введите текст для шифрования: ')
            text = refactor_str_to_enc(text)
            text = add_to_text_itself(text, size)
            print("Шифрование: ", encrypt(text, key, size))
        elif big_or_small == 2:
            file = open('test.txt', 'r',  encoding='UTF8')
            text = refactor_str_to_enc(file.read())
            text = add_to_text_itself(text, size)
            f = open('output.txt', 'w',  encoding='UTF8')
            f.write(encrypt(text, key, size))
            print("Результат работы в файле output.txt")
    elif what_to_do == 2:
        if big_or_small == 1:
            text = input('Введите текст для расшифрования: ')
            print("Расшифрование: ", refactor_str_from_enc(del_text_from_itself(decrypt(text, key, size))))
        elif big_or_small == 2:
            file = open('output.txt', 'r',  encoding='UTF8')
            text = file.read()
            f = open('new.txt', 'w',  encoding='UTF8')
            f.write(refactor_str_from_enc(del_text_from_itself(decrypt(text, key, size))))
            print("Результат работы в файле new.txt")
